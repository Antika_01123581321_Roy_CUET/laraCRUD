@extends('../master')


@section('title','Book Title - Active List')


@section('content')


<div class="container">


        <div class="navbar">

    <a href="create"><button type="button" class="btn btn-primary btn-lg">Create New</button></a>

        </div>


        {!! Form::open(['url' => '/BookTitle/search_result']) !!}
            {!! Form::text('keyword') !!}
            {!! Form::submit('Search', [ 'class'=>'btn btn-success']) !!}
            {!! Form::close() !!}

    Total: {!! $allData->total() !!} Book(s) <br>
            Showing: {!! $allData-> count() !!} Book(s) <br>

        {!! $allData->links() !!}


            <table class="table table-bordered table-striped">

                <tr>

                    <th> Book Title </th>
                    <th> Author Name </th>
                    <th> Action Buttons </th>

                </tr>

                @foreach($allData as $oneData)

                    <tr>

                        <td>   {!! $oneData['book_title'] !!}</td>
                        <td>   {!! $oneData['author_name'] !!}</td>


                        <td>


                            <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">VIEW</button></a>
                            <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">EDIT</button></a>
                            <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">DELETE</button></a>

                        </td>

                    </tr>

            @endforeach
            </table>


            {!! $allData->links() !!}




</div>

@endsection