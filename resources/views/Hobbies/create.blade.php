@extends('../master')

@section('title','Hobbies - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Hobbies/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('hobbies','Hobbies:') !!}<br>

            {!! Form::label('hobbies','Sleeping:') !!}
            {!! Form::checkbox('hobbies[]', 'Sleeping' ) !!}<br>

            {!! Form::label('hobbies','Photography:') !!}
            {!! Form::checkbox('hobbies[]', 'Photography' ) !!}<br>

            {!! Form::label('hobbies','Kayaking:') !!}
            {!! Form::checkbox('hobbies[]', 'Kayaking' ) !!}<br>

            {!! Form::label('hobbies','Treking:') !!}
            {!! Form::checkbox('hobbies[]', 'Treking' ) !!}<br>

            {!! Form::label('hobbies','Watching Movies:') !!}
            {!! Form::checkbox('hobbies[]', 'Watching Movies' ) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
