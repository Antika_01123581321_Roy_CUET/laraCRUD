@extends('../master')

@section('title','Email - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Email - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Email/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('email','email:') !!}
            {!! Form::email('email','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
