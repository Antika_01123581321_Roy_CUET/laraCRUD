<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/foo', function () {
    return "I am antika";
});





Route::get('/BookTitle/create', function () {
    return view('BookTitle/create');
})-> name('BookTitleCreate');


Route::post('/BookTitle/store', ['uses'=>'BookTitleController@store']);

Route::get('/BookTitle/index', ['uses'=>'BookTitleController@index'])-> name('index');


Route::get('/BookTitle/view/{id}', ['uses'=>'BookTitleController@view']);

Route::get('/BookTitle/edit/{id}', 'BookTitleController@edit');


Route::post('/BookTitle/update', ['uses'=>'BookTitleController@update']);
Route::get('/BookTitle/delete/{id}', ['uses'=>'BookTitleController@delete']);



Route::post('BookTitle/search_result', function(){

    $path = 'BookTitle/search/'. $_POST['keyword'];

    return redirect($path);

});


Route::get('/BookTitle/search/{keyword}', ['uses'=>'BookTitleController@search'])->name('searchindex');


Route::get('/BookTitle/search/',function (){
    return redirect()->route('index');
});



Route::get('/Birthday/create', function () {
    return view('Birth_Day/create');
})-> name('BirthdayCreate');

Route::post('/Birth_Day/store', ['uses'=>'BirthDayController@store']);





Route::get('/City/create',function (){ return view('City/create'); } )->name('CityCreate');

Route::post('/City/store', ['uses'=>'CityController@store']);




Route::get('/Email/create',function (){ return view('Email/create'); } )->name('EmailCreate');

Route::post('/Email/store', ['uses'=>'EmailController@store']);




Route::get('/Gender/create',function (){ return view('Gender/create'); } )->name('GenderCreate');
Route::post('/Gender/store', ['uses'=>'GenderController@store']);






Route::get('/Hobbies/create',function (){return view('Hobbies/create');} )->name('HobbiesCreate');
Route::post('/Hobbies/store', ['uses'=>'HobbiesController@store']);







Route::get('/Profile_Picture/create',function (){return view('Profile_Picture/create');} )->name('ProfilePictureCreate');
Route::post('/Profile_Picture/store', ['uses'=>'ProfilePictureController@store']);







Route::get('/Summary_Of_Organization/create',function (){return view('Summary_Of_Organization/create');} )->name('SummaryOfOrganizationCreate');
Route::post('/Summary_Of_Organization/store', ['uses'=>'SummaryOfOrganizationController@store']);