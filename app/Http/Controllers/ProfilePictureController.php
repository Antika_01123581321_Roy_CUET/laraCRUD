<?php

namespace App\Http\Controllers;

use App\ProfilePicture;
use Illuminate\Http\Request;

class ProfilePictureController extends Controller
{
    public function store(){


        $objModel = new ProfilePicture();
        $objModel->name=$_POST['name'];
        $objModel->profile_picture=$_POST['profile_picture'];
        $status=$objModel->save();
        return redirect()->route('ProfilePictureCreate');

    }
}
