<?php

namespace App\Http\Controllers;

use App\Hobbies;
use Illuminate\Http\Request;

class HobbiesController extends Controller
{
    public function store(){




        $objModel = new Hobbies();
        $objModel->name=$_POST['name'];
        $strHobbies = implode(",",$_POST["hobbies"]);
        $_POST["hobbies"] = $strHobbies;
        $objModel->hobbies=$_POST["hobbies"];
        $status=$objModel->save();
        return redirect()->route('HobbiesCreate');

    }
}
