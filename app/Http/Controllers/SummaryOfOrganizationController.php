<?php

namespace App\Http\Controllers;

use App\SummaryOfOrganization;
use Illuminate\Http\Request;

class SummaryOfOrganizationController extends Controller
{
    public function store(){

        $objModel = new SummaryOfOrganization();
        $objModel->name=$_POST['name'];
        $objModel->summary=$_POST['summary'];
        $status=$objModel->save();
        return redirect()->route('SummaryOfOrganizationCreate');
    }

}


