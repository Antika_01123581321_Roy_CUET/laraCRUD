<?php

namespace App\Http\Controllers;

use App\Gender;
use Illuminate\Http\Request;

class GenderController extends Controller
{
  public function store(){

      $objModel = new Gender();
      $objModel->name=$_POST['name'];
      $objModel->gender=$_POST['gender'];
      $status=$objModel->save();
      return redirect()->route('GenderCreate');

  }
}
