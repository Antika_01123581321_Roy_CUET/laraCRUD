<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilePicture extends Model
{
    public  $timestamps= false;
    protected $fillable = [
        'name', 'profile_picture'
    ];


}
