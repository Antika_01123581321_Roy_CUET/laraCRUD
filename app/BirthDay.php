<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthDay extends Model
{

    public  $timestamps= false;
    
    protected $fillable = [
        'name', 'birthday'
    ];

}
